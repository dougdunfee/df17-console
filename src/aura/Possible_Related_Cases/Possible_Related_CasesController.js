({
    tabFocused: function(component, event, helper) {
        let barAPI = component.find('utilitybar');
        let workspaceAPI = component.find('workspace');

        workspaceAPI.getFocusedTabInfo().then(function(response) {
            component.find("caseRecordHandler").reloadRecord();
        })
    },
    handleRecordUpdated: function(component, event, helper) {
        let barAPI = component.find('utilitybar');

        let record = component.get('v.record');

        if (record && record.fields && record.fields.Status && record.fields.Status.value === 'Cross Sell') {
            barAPI.setUtilityHighlighted({
                highlighted: true
            });
            setTimeout(function() {
                barAPI.setUtilityHighlighted({
                    highlighted: false
                });
            }, 10000);
        } else {
            barAPI.setUtilityHighlighted({
                highlighted: false
            });
        }
    }
})