({
	// This is DEMO Magic
	maybePrefill : function(component, event, helper) {
		// If the shift key was held down, prefill for Research Medical Center
		if(event.shiftKey) {
			if(event.altKey) {
				component.set('v.name', 'Texas Children\'s Hospital');
				component.set('v.street', '6621 Fannin St');
				component.set('v.suite', '');
				component.set('v.city', 'Houston');
				component.set('v.state', 'TX');
				component.set('v.zipcode', '77030');
			}
			else {
				component.set('v.name', 'Research Medical Center');
				component.set('v.street', '2316 E Meyer Blvd');
				component.set('v.suite', '');
				component.set('v.city', 'Kansas City');
				component.set('v.state', 'MO');
				component.set('v.zipcode', '64132');
			}
			
		}
	}
})