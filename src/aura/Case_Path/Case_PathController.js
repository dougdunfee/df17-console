({
    doInit: function(component, event, helper) {
        //initialize and set the status list
        let statusList = ['New', 'Working', 'Escalated', 'Cross Sell', 'Closed'];
        component.set('v.statusList', statusList);
    },

    handleRecordUpdated: function(component, event, helper) {
        //This section below is needed for the force:recordData logic
        let eventParams = event.getParams();

        if (eventParams.changeType === 'LOADED') {
            //first time loading so lets set the currentIndex
            let currStatList = component.get('v.statusList');
            let currStatus = component.get('v.record').fields.Status.value;
            component.set('v.currentIndex', currStatList.indexOf(currStatus));
        } else if (eventParams.changeType === 'CHANGED' && eventParams.changedFields.Status != null) {
            let currStatList = component.get('v.statusList');
            let currStatus = eventParams.changedFields.Status.value;
            component.set('v.currentIndex', currStatList.indexOf(currStatus));

            //this will open the cross sell utility
            if (currStatus === 'Cross Sell') {
                helper.openCrossSellUtility();
            }
        }
    },
    crossSellComplete: function(component, event, helper) {
        let recordID = component.get('v.recordId');
        let record = component.get('v.record');
        if (event.getParam('recordID') === recordID) {
            event.stopPropagation();
            record.fields.Status.value = 'Closed';
            component.find("caseRecordHandler").saveRecord($A.getCallback(function(saveResult) {
                console.log(saveResult);
            }));
        }
    }
})