({
	becomePreferredSize : function(component, event, helper) {
		var panelHeight = component.get("v.panelHeight"),
			panelWidth  = component.get("v.panelWidth"),
			utilityBar  = component.find('utilityBar'),
			isCommunity = component.get("v.runAsCommunityUser");
	
		// TODO: Should handle non-PX dimensions, e.g. REM and EM
		if( isCommunity === false ) {
			utilityBar.setPanelHeight({ 'heightPX' : panelHeight });
			utilityBar.setPanelWidth({ 'widthPX' : panelWidth });
		}
	}
})