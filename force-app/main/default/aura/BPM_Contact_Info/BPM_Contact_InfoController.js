({
	// This is DEMO Magic
	maybePrefill : function(component, event, helper) {
		// If the shift key was held down, prefill for Adrian Gonzalez
		if(event.shiftKey) {
			component.set('v.firstName', 'Adrian');
			component.set('v.lastName', 'Gonzalez');
			component.set('v.phone', '913-555-0412');
			component.set('v.email', 'adrian@rmc.demo');
		}
	}
})