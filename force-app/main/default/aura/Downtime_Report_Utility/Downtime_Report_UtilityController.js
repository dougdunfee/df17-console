({
    doInit: function(component, event, helper) {
        //initialize the attributes on the component
        let now = new Date();
        component.set("v.timeStamp", now.toISOString());
        component.set("v.isModal", false);
        component.set("v.submissionCount",0);
    },
    submit: function(component, event, helper) {
        //get the utility bar object for our calls
        let utilityAPI = component.find("utilitybar");
        
        //the next two calls the the panel icon and header
        utilityAPI.setPanelHeaderIcon({
            icon: "check"
        });
        
        utilityAPI.setPanelHeaderLabel({
            label: "Submitting Report"
        });
        
        //after 5 seconds, minimize the utility
        //change the minimized label and icon
        //after another 5 seconds reset the panel header label and icon
        //set the minimized label and icon to show the count
        setTimeout(function() {
           	utilityAPI.minimizeUtility();
            
            utilityAPI.setUtilityLabel({
            	label: "Report Submitted"
            });
            utilityAPI.setUtilityIcon({
                icon: "check"
            });
            
            setTimeout(function() {
                utilityAPI.setPanelHeaderLabel({
                    label: "Downtime Reporter"
                });
                utilityAPI.setPanelHeaderIcon({
                    icon: "touch_action"
                });
                
                let submissionCount = component.get("v.submissionCount");
                submissionCount++;
                
                utilityAPI.setUtilityLabel({
                    label: "Total Reports Submitted : " + submissionCount
                });
                utilityAPI.setUtilityIcon({
                    icon: "touch_action"
                });
                
                component.set("v.submissionCount", submissionCount);
                component.set("v.timeStamp", null);
                component.set("v.comments", null);
            }, 5000);
        }, 5000);
    },
    
    
    toggleModal : function(component, event, helper) {
        //method that toggles the modal functionality on and off
        let utilityAPI = component.find("utilitybar");
        let isModal = component.get("v.isModal");
        
        utilityAPI.toggleModalMode({
            enableModalMode: !isModal
        });
        
        component.set("v.isModal",!isModal);
    }
})