({
    doInit: function(component, event, helper) {

        let action = component.get("c.getRelatedProductList");
        let recordId = component.get("v.recordId");

        action.setParams({
            newCaseId: recordId
        });

        action.setCallback(this, function(response) {
            let state = response.getState();
            if (state == "SUCCESS") {
                component.set("v.productList", response.getReturnValue());
                component.get("v.productList");
            }
        });
        $A.enqueueAction(action);
    },

    productClicked: function(component, event, helper) {
        console.log(event);
        let productId = event.target.getAttribute('data-product-id');
        let productName = event.target.innerText;
        let workspaceAPI = component.find("workspace");

        workspaceAPI.getFocusedTabInfo()
            .then(function(response) {
                let parentTab;
                if (response.isSubtab === true) {
                    parentTab = response.parentTabId;
                } else {
                    parentTab = response.tabId;
                }
                workspaceAPI.openSubtab({
                    parentTabId: parentTab,
                    url: '#/sObject/' + productId + '/view',
                    focus: true
                });
            });
    }
})