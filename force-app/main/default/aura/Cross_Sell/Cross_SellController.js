({
    
    tabFocused: function(component, event, helper) {
        //When the tab is focused we call the workspaceAPI
        //to wait until it has been fully loaded, then we
        //reload the record to check the status of the tab
        let workspaceAPI = component.find('workspace');

        workspaceAPI.getFocusedTabInfo().then(function(response) {
            component.find("caseRecordHandler").reloadRecord();
        })
    },
    handleRecordUpdated: function(component, event, helper) {
        //this will get invoked when the current record gets updated
        //we use this to check the status and to highlight the utility
        //if the status is "Cross Sell"
        let barAPI = component.find('utilitybar');

        let record = component.get('v.record');

        if (record && record.fields && record.fields.Status && record.fields.Status.value === 'Cross Sell') {
            barAPI.setUtilityHighlighted({
                highlighted: true
            });
            setTimeout(function() {
                barAPI.setUtilityHighlighted({
                    highlighted: false
                });
            }, 10000);
        } else {
            barAPI.setUtilityHighlighted({
                highlighted: false
            });
        }
    },
    openCrossSellUtility: function(component, event, helper) {
        //function that listens for the open cross sell event
        //and then opens the utility
        let barAPI = component.find('utilitybar');
        barAPI.openUtility();
    },

    markComplete: function(component, event, helper) {
        //function to emit a cross sell complete event
        let barAPI = component.find('utilitybar');
        let workspaceAPI = component.find('workspace');

        workspaceAPI.getFocusedTabInfo().then(function(response) {
            let crossSellEvent = $A.get('e.c:CrossSellComplete');
            crossSellEvent.setParams({
                recordID: response.recordId
            });
            crossSellEvent.fire();
            barAPI.minimizeUtility();
        })
    }
})