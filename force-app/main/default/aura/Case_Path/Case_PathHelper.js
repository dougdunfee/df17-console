({
    openCrossSellUtility: function() {
        //fires the open cross sell utility event
        let openEvent = $A.get('e.c:Open_Cross_Sell_Utility');
        openEvent.fire();

    }
})